CREATE DATABASE blog_db;

CREATE TABLE tbl_posts (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id)
    CONSTRAINT fk_posts_user_id
    FOREIGN KEY (user_id) REFERENCES tbl_users (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
);

CREATE TABLE tbl_post_comments (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_comments_post_id
    FOREIGN KEY (post_id) REFERENCES tbl_posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
    CONSTRAINT fk_post_comments_USER_id
    FOREIGN KEY (user_id) REFERENCES tbl_users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
);

CREATE TABLE tbl_users(
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(300) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE tbl_post_likes(
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_post_likes_user_id
    FOREIGN KEY (user_id) REFERENCES tbl_users(id)
    ON UPDATE CASCADE 
    ON DELETE RESTRICT,
    CONSTRAINT fk_post_likes_post_id
    FOREIGN KEY (post_id) REFERENCES tbl_post(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

INSERT INTO tbl_users (email, password, datetime_created) 
VALUES ("johnsmith@gmail.com", "passwordA" , "2021-1-1 01:00:00"),
("juandelacruz@gmail.com", "passwordB", "2021-1-1 02:00:00"),
("janesmith@gmail.com", "passwordC", "2021-1-1 03:00:00"),
("mariadelacruz@gmail.com", "passwordD", "2021-1-1 04:00:00"),
("johndoe@gmail.com", "passwordE", "2021-1-1 05:00:00");

INSERT INTO tbl_posts (title, content, datetime_posted)  
VALUES ("First Code", "Hello World!" , "2021-1-1 01:00:00"),
("Second Code", "Hello Earth!", "2021-1-1 02:00:00"),
("Third Code", "Welcome to Mars!", "2021-1-1 03:00:00"),
("Fourth Code", "Bye bye solar system", "2021-1-1 04:00:00");


